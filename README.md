# Documentació M17UF2R1-MontoliuSandra

-- Controls
    Utilitza [W,A,S,D] o les fletxes per a desplaçarte per el mapa.
    El punter del ratolí per aupuntar, click esquerra per a disparar.
    [X, C] o la roda del ratolí per a canviar d'arma.
    [E] per a utilitzar les portes.

-- Entrega 1 ( * = Implementat en seguents entregues)
    R1.1
        1. El jugador es mou lliurement per el pla.
        2. Animacions: El personatge fa l'animació de caminar en 8 posibles direccions. 
        3. El personatge canvia d'orientació segons la posició del ratolí a la pantalla.
        4.1.1. Col·lecció de 5 armes. No hi ha inventari definitiu, l'inventari provisional es crea agafant les 5 armes creades en ScriptableObject i les afegeix totes directament.
        4.1.2. Es pot canviar d'arma amb la rodeta del ratolí o prement  X i C.
        4.2.1. Atac a distancia: La bala surt disparada desde l'arma cap a on es troba el punter del ratolí, segons l'arma canvia la velocitat de les bales, el mal que fan (no acabat d'implementar), la seva cadencia de dispar (no acabat d'implementar) i el numero de bales que tenen (no acabat d'implementar).
        4.2.3. Dash no implementat.
    R1.2
       *1. Barra de vida no implementada.
       *2.1. Escenari per a jugar no implementat.
        2.3. Escenari prefab per a proves creat.
        2.4. L'escenari creat te parets no destruibles i que el jugador no pot atavesar.
        2.5. Elements destruibles no implementats.
        3.1. Armes creades amb ScriptableObjects amb la posibilitat d'utilitzar-les desde l'inventari, però no apareixen com a GameObjects (items) dintre del joc per a poder agafar-les. Les armes tenen valor de bales pero no es resten ni es controla que hi hagi abans de disparar.
        3.2 Item per augmentar la vida no implementat.
    R1.3
        1. Spawner d'enemics no implementat.
        2.1. Enemic que segueix al personatge no acabat d'implementar. Segueix al personatge però només segueix al jugador, no sap evitar les parets al xocar.
        *2.2. Enemic que dispara no implementat.
    R1.4
        1.1. El jugador comença amb les 5 armes i la seva munició tampoc está limitada.
        1.2. No hi ha "ones" d'enemics ja que el spawner no está implementat.
        1.3. No es controla la mort de tots els enemics per a poder acabar el joc.
    R1.5
        *1. Manager tipus Singleton no implementat.
        *2. Màquina d'estats no implementada.
        3. Herencies no implementades.
        4. Utilització de ScriptableObject: Armes
        5. Temática i estética 2D pixelart.
        *6. Documentació no feta.

-- Entrega 2
    R1.1
        4.2.1. 
    R1.2
        1. Barra de vida implementada. Mostra la vida actual del jugador (de manera escrita i amb una barra) segons el seu ScriptableObject, el tamany de la barra varia i canvia de color. 
        1.3. Augmenta una puntuació al donar-li als enemics.
        2.1 Escenari amb minim dues portes implementat.
    R1.3
        1. Spawner implementat. Es crea un número determinat d'enemics (5 Shooters) en forma d'onades (2 onades per nivell).
        2.2. Enemic que dispara implementat.
             Al inicialitzar-los apareixerán en una posició aleatoria del mapa, després, agafarant una posició alaetoria del tilemap de "posicions per a disparar" i es dirigirán allà creant un camí desde la seva posició actual. Un cop arribi a la seva posició l'enemic "montará" la seva torreta per a poder disparar (no pot disparar fins arribar a la posició definida). Quan el jugador entri dintre del seu radi l'enemic el seguirá apuntant i disparant. Si els mates mentres están en moviment cosegueixes 5 punts, mentres si estan quiets son 10 punts.
    R1.5 
        1. Implementat manager Singleton: GameManager
        2. Implementada m'àquina d'estats: EnemyShooter
        6. Omplint el README
    R2.1
        1. Us de ScriptableObjects: Armes, Player, Inventari i Dades de persistencia.
        2. 
        3.
    R2.2 Habitació de botiga implementada, apareix cada 3 nivells superats.
        1.
        2.
    R2.3.
        1. Persistencia entre sessions.
    R2.4.
        1.
    R2.5
        1.
        2.
        3.
    R2.6
        1. Creació de 2 nivells diferents.
        2. Generació del seguent nivell aleatori, el mapa s'escollirá aleatoriament entre els creats.
        3. El spawner s'adapta a la forma dels nivells.
        4. Sumes puntuació al acabar una sala.
        5. S'ha de traspasar una porta per a passar al seguent nivell, aquesta estará tancada fins que es matin tots els enemics del nivell.
    R2.7
        1. A la pantalla d'inici es mostra la millor puntuació fins al moment i la puntuació de l'ultima partida jugada. També hi ha els botons de Jugar i Sortir.
        2. 
        3. A la pantalla de derrota es mostra la puntuació final, si es la millor puntuació fins al moment ho indica, mostra el número d'enemics eliminats i el número de nivells superats.
    
