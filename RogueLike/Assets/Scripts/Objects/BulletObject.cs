using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "New Bullet", menuName = "Game Objects")]
public class BulletObject : ScriptableObject
{
    public Sprite Sprite;
}
