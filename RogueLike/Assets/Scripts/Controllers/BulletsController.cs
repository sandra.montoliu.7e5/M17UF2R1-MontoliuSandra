using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Experimental.GraphView.GraphView;

public enum Parent
{
    Player,
    EnemyShooter
}

public class BulletsController : MonoBehaviour
{
    private Vector3 _mousePosition;
    private float _angleMousePlayer;
    private Vector3 _playerPosition;

    private Rigidbody2D _rigidbody;

    private float _speedProjectiles;

    public Parent Parent;
    private float _angleEnemyPlayer;
    private Vector3 _direction;
    

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = gameObject.GetComponent<Rigidbody2D>();


        switch (Parent)
        {
            case Parent.Player:
                BulletsPlayer();
                break;
            case Parent.EnemyShooter:
                BulletsEnemy();
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.layer)
        {
            case 8: // 8 = Wall
                Object.Destroy(this.gameObject);
                break;
        }
    }

    private void GetData()
    {

    }

    private void BulletsPlayer()
    {
        _mousePosition = Input.mousePosition;
        _playerPosition = Camera.main.WorldToScreenPoint(transform.position);
        _mousePosition.x -= _playerPosition.x;
        _mousePosition.y -= _playerPosition.y;
        _angleMousePlayer = Mathf.Atan2(_mousePosition.y, _mousePosition.x) * Mathf.Rad2Deg;


        
        _speedProjectiles = GameObject.Find("Player(Clone)").GetComponent<PlayerController>().ActualWeaponSO.SpeedProjectiles;

        _rigidbody.AddForce(new Vector2(_mousePosition.x * _speedProjectiles, _mousePosition.y * _speedProjectiles));
    }
    
    private void BulletsEnemy()
    {
        _speedProjectiles = 80f;
        _playerPosition = GameObject.Find("Player(Clone)").transform.position;
        _direction = _playerPosition - transform.position;
        _angleEnemyPlayer = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg;

        _rigidbody.AddForce(new Vector2(_direction.x * _speedProjectiles, _direction.y * _speedProjectiles));
    }
}
