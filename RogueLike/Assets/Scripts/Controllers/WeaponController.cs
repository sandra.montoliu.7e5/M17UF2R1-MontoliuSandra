using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    // Rotation Weapon
    private Vector3 _mousePosition;
    private float _angleMousePlayer;
    private Vector3 _playerWorldPosition;

    /*public float _radiusPlayerWeapon = 1f;
    private float _distancePlayerWeapon;
    private Vector3 _playerPosition;
    private Vector3 _playerWeapon;*/

    // Shoot
    public GameObject BulletPrefab;

    // Start is called before the first frame update
    void Start()
    {
        BulletPrefab.GetComponent<BulletsController>().Parent = Parent.Player;
    }

    // Update is called once per frame
    void Update()
    {
        Moviment();
    }

    private void Moviment()
    {
        // Rotate object
        _mousePosition = Input.mousePosition;  // Vector3 de la posici� actual del ratol�.
        _playerWorldPosition = Camera.main.WorldToScreenPoint(transform.position); // Posici� del player segons la seva posicio a la pantalla, no al mon.
        _mousePosition.x -= _playerWorldPosition.x;
        _mousePosition.y -= _playerWorldPosition.y;
        _angleMousePlayer = Mathf.Atan2(_mousePosition.y, _mousePosition.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0,0,_angleMousePlayer));

        // Rotation Weapon around Player
        /*_playerPosition = GameObject.Find("Player").transform.position;
        _distancePlayerWeapon = Vector3.Distance(transform.position, _playerPosition);
        if (_distancePlayerWeapon != _radiusPlayerWeapon)
        {
            _playerWeapon = transform.position - _playerPosition;
            _playerWeapon *= _radiusPlayerWeapon / _distancePlayerWeapon;
            transform.position = _playerPosition + _playerWeapon; 
        }*/

        // Flip Sprite && OrderInLayer
        if (_angleMousePlayer > 90 || _angleMousePlayer < -90)
        {
            this.GetComponent<SpriteRenderer>().flipY = true;
            this.GetComponent<Renderer>().sortingOrder = 1;
        }
        else
        {
            this.GetComponent<SpriteRenderer>().flipY = false;
            this.GetComponent<Renderer>().sortingOrder = 2;
        }
    }

    public void Shoot()
    {
        BulletPrefab.GetComponent<BulletsController>().Parent = Parent.Player;
        Instantiate(BulletPrefab, transform.position, new Quaternion(0f, 0f, 0f, 0f));
    }
}
