using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorController : MonoBehaviour
{

    public bool _doorInteractuable;


    // Start is called before the first frame update
    void Start()
    {
        _doorInteractuable = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GetComponent<SpriteRenderer>().enabled && Input.GetKeyDown("e") && _doorInteractuable) EnterDoor();
    }

    private void EnterDoor()
    {
        if (SceneManager.GetActiveScene().name == "ShopScene") SceneManager.LoadScene("GameScene"); 
        else if ((Resources.Load<DataPersistenceScriptableObject>("Data").Level % 3) == 0)
        {       // Enviar a tienda, pero de momento.
            Resources.Load<DataPersistenceScriptableObject>("Data").Level++;
            Resources.Load<DataPersistenceScriptableObject>("Data").ActualGamePoints += 20;
            SceneManager.LoadScene("ShopScene");
        }
        else
        {
            Resources.Load<DataPersistenceScriptableObject>("Data").Level++;
            Resources.Load<DataPersistenceScriptableObject>("Data").ActualGamePoints += 20;
            SceneManager.LoadScene("GameScene");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.layer)
        {
            case 6: // 6 = Player
                    //Mostrar icono para interactuar.
                _doorInteractuable = true;
                break;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        switch (collision.gameObject.layer)
        {
            case 6: // 6 = Player
                    //Mostrar icono para interactuar.
                _doorInteractuable = true;
                break;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        switch (collision.gameObject.layer)
        {
            case 6: // 6 = Player
                    //Mostrar icono para interactuar.
                _doorInteractuable = false;
                break;
        }
    }
}
