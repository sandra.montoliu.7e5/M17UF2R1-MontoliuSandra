using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    private GameManager gameManager;
    private List<Vector3> _spawnerPositions;
    private int _indexPositions;
    public int _maxEnemys;
    public int _enemysInScene;
    private int _maxNumWave;
    private int _numWave;
    [SerializeField] private GameObject _enemyShooter;

    [SerializeField] private GameObject _doors;
    private bool _doorInteractuable;


    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _spawnerPositions = gameManager.GetTilesPositions("Tilemap_Ground");
        _maxNumWave = 2;
        _maxEnemys = 5;
        _doors = GameObject.Find("Doors");
        _doorInteractuable = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (_enemysInScene <= 0 && _numWave < _maxNumWave)
        {
            SpawnEnemys();
            _numWave++;
        }
        else if (_enemysInScene <= 0 && _numWave >= _maxNumWave && !_doorInteractuable) OpenDoor();
        
    }

    private void SpawnEnemys()
    {
        for (int i = 0; i < _maxEnemys; i++)
        {
            _indexPositions = Random.Range(0, _spawnerPositions.Count - 1);
            _enemysInScene++;
            Instantiate(_enemyShooter, _spawnerPositions[_indexPositions], new Quaternion (0,0,0,0));
        }
    }

    private void OpenDoor()
    {
        for (int i = 0; i < _doors.transform.childCount; i++)
        {
            if (_doors.transform.GetChild(i).gameObject.activeSelf)
            {
                _doors.transform.GetChild(i).gameObject.GetComponent<SpriteRenderer>().enabled = false;
            }
        }
    }
}
