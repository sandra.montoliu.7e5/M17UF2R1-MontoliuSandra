using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum EnemyFase
{
    Moving,
    Standing,
    Shooting
}

public class EnemyShooterController : MonoBehaviour
{
    private GameManager gameManager;
    private List<Vector3> _positionsToShoot;
    private int _indexPositionsToShoot;
    [SerializeField] private Vector3 _positionToShoot;

     private int[,] _matrixPathFinder;
     [SerializeField] private List<Vector3> _pathNavigator;
     private Tilemap _mapWalls;
     private Vector3 _mapMin;
     private int _mapX;
     private int _mapY;
     private TileBase _tile;

     private Tilemap _mapGround; // Asegurate de que miden lo mismo los dos tilemaps

    private float _movementSpeed;
    private Vector3 _nextPosition;

    public Object BulletPrefab;
    [SerializeField] private EnemyFase _enemyFase;
    private Vector3 _direction;
    private Transform _player;
    private float _angle;

    private Rigidbody2D _rigidbody;
    /*private Ray2D _rayShoot;
    private RaycastHit2D _raycastHit;
    private float _rangeRay;
    private Vector3 _directionRay;*/

    private float _timer;

    private float _life = 25;
    private BoxCollider2D _boxCollider;

    private SpawnerController _spawnerController;

    // Start is called before the first frame update
    void Start()
    {
        _positionToShoot = GetPositionToShoot();
        _mapWalls = GameObject.Find("Tilemap_Walls").GetComponent<Tilemap>();
        _mapGround = GameObject.Find("Tilemap_Ground").GetComponent<Tilemap>();
        _mapMin = _mapWalls.localBounds.min;

        PathFinder();
        _movementSpeed = 5f;
        _nextPosition = new Vector3();

        GetPath(transform.position, _positionToShoot);

        _enemyFase = EnemyFase.Moving;
        _rigidbody = this.GetComponent<Rigidbody2D>();
        /*_rangeRay = 5f;*/
        _boxCollider = this.GetComponent<BoxCollider2D>();

        _spawnerController = GameObject.Find("GameManager").GetComponent<SpawnerController>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (_enemyFase)
        {
            case EnemyFase.Moving:
                RunPath(_pathNavigator);
                break;
            case EnemyFase.Standing:
                break;
            case EnemyFase.Shooting:
                TurnAround();
                Shoot();
                break;
        }

        if (_life <= 0) Die();
    }

    Vector3 GetPositionToShoot() // Retorna una posicio aleatoria d'una de les tiles.
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _positionsToShoot = new List<Vector3>(gameManager.GetTilesPositions("Tilemap_ShootersPosition"));
        _indexPositionsToShoot = Random.Range(0, _positionsToShoot.Count - 1);
        return _positionsToShoot[_indexPositionsToShoot];
    }

    void PathFinder()
    {
        _matrixPathFinder = new int[_mapWalls.size.x, _mapWalls.size.y];
        _pathNavigator = new List<Vector3>();

        _mapMin = _mapWalls.localBounds.min;

        for (int x = 0; x < _mapWalls.size.x; x++) 
        {
            for (int y = 0; y < _mapWalls.size.y; y++) 
            {
                // Encuentra la posicion del tile en la esquina inferior. Es suma perque _mapMin es negatiu;
                _mapX = (int) (x + _mapMin.x);
                _mapY = (int) (y + _mapMin.y);
                
                _tile = _mapWalls.GetTile(new Vector3Int(_mapX, _mapY,0));

                // Asignem numeros segons tile
                if (_tile != null) // Hi ha paret
                {
                    //_matrixPathFinder[_matrixPathFinder.GetLength(0) - j -1, i] = -1;
                    _matrixPathFinder[x, y] = -1;
                }
                else if (_mapGround.GetTile(new Vector3Int(_mapX, _mapY, 0)) != null) // Hi ha terra
                {
                    //_matrixPathFinder[_matrixPathFinder.GetLength(0) - j - 1, i] = 0;
                    _matrixPathFinder[x, y] = 0;
                }
                else // No hi ha ni paret ni terra
                {
                    //_matrixPathFinder[_matrixPathFinder.GetLength(0) - j -1, i] = -1;
                    _matrixPathFinder[x, y] = -1;
                }                
                
            }
        }
    }

    /*private void DrawMatrix()
    {
        // Dibuixar matriu
        string _matrix = "";
        int z = -1;
        foreach (var num in _matrixPathFinder)
        {
            z++;
            //if (num == -1) _matrix += "# ";
            //else if (num == 0) _matrix += "= ";
            _matrix += num + " ";
            if (z >= _matrixPathFinder.GetLength(1) - 1)
            {
                z = -1;
                _matrix += "\n";
            }
        }
        Debug.Log(_matrixPathFinder.GetLength(0));
        Debug.Log(_matrixPathFinder.GetLength(1));
        Debug.Log(_matrix);
    }*/

    Vector3Int ToLocal(Vector3 world)
    {
        var local = (world - _mapWalls.transform.position) - _mapWalls.localBounds.min;
        return new Vector3Int((int)local.x, (int)local.y);
    }
    Vector3 ToGlobal(Vector3Int local)
    {
        var f_local = new Vector3(local.x, local.y, 0);
        return f_local + _mapWalls.transform.position + _mapWalls.localBounds.min;
    } 
    Vector3 ToGlobal(Vector3 local)
    {
        return local + transform.position + _mapWalls.localBounds.min;
    }
    private List<Vector3> GetPath(Vector3 start, Vector3 end)
    {

        var lStart = ToLocal(start);
        var lEnd = ToLocal(end);
        GetPathUsingLocalCoordinates(lStart, lEnd);

        for (int i = 0; i < _pathNavigator.Count; i++)
        {
            var loc = _pathNavigator[i] + Vector3.one * 0.5f;
            loc.z = 0;
            _pathNavigator[i] = loc;
        }

        return _pathNavigator;
    }

    private void GetPathUsingLocalCoordinates(Vector3Int localStart, Vector3Int localTarget)
    {
        // Netejar matriu
        for (int x = 0; x < _mapWalls.size.x; x++)
        {
            for (int y = 0; y < _mapWalls.size.y; y++)
            {
                if (_matrixPathFinder[x, y] != -1) _matrixPathFinder[x, y] = 0;
            }
        }
        // Netejar cami
        _pathNavigator.Clear();

        if (_matrixPathFinder[localTarget.x, localTarget.y] != 0 || _matrixPathFinder[localStart.x, localStart.y] != 0) return;



        int distance = 1;

        Queue<Vector3Int> visitedCells = new Queue<Vector3Int>();
        _matrixPathFinder[localTarget.x, localTarget.y] = distance;
        visitedCells.Enqueue(localTarget);

        while (visitedCells.Count != 0 && visitedCells.Count < 1000)
        {
            distance += 1;
            var cell = visitedCells.Dequeue();
            Visit(visitedCells, distance, cell.x, cell.y + 1); // Up
            Visit(visitedCells, distance, cell.x, cell.y - 1); // Down
            Visit(visitedCells, distance, cell.x + 1, cell.y); // Right
            Visit(visitedCells, distance, cell.x - 1, cell.y); // Left
        }
        ComputePath(localStart);
    }

    private void Visit(Queue<Vector3Int> visitedCells, int distance, int x, int y)
    {
        if (x < 0 || x >= _mapWalls.size.x) return;
        if (y < 0 || y >= _mapWalls.size.y) return;
        if (_matrixPathFinder[x, y] == 0)
        {
            _matrixPathFinder[x, y] = distance;
            visitedCells.Enqueue(new Vector3Int(x, y, 0));
        }
    }

    private void ComputePath(Vector3Int localStart)
    {
        Vector3Int currentLocalTile = localStart;
        bool working = true;

        while (working)
        {
            _pathNavigator.Add(ToGlobal(currentLocalTile));
            var (x, y) = ((int)currentLocalTile.x, (int)currentLocalTile.y);
            var d = _matrixPathFinder[x, y];

            working = false;
            if (_matrixPathFinder[x, y + 1] < d && _matrixPathFinder[x, y + 1] != -1)
            {
                currentLocalTile.Set(x, y + 1, 0);
                working = true;
                continue;
            }
            if (_matrixPathFinder[x, y - 1] < d && _matrixPathFinder[x, y - 1] != -1)
            {
                currentLocalTile.Set(x, y - 1, 0);
                working = true;
                continue;
            }
            if (_matrixPathFinder[x + 1, y] < d && _matrixPathFinder[x + 1, y] != -1)
            {
                currentLocalTile.Set(x + 1, y, 0);
                working = true;
                continue;
            }
            if (_matrixPathFinder[x - 1, y] < d && _matrixPathFinder[x - 1, y] != -1)
            {
                currentLocalTile.Set(x - 1, y, 0);
                working = true;
                continue;
            }
        }
    }


    void RunPath(List<Vector3> path)
    {
        if (path.Count > 0)
        {
            if (_nextPosition != path[0]) _nextPosition = path[0];
            float percent = Time.deltaTime * _movementSpeed;
            transform.position = Vector3.MoveTowards(transform.position, _nextPosition, percent);

            if (transform.position == _nextPosition)
            {
                path.RemoveAt(0);
            }
        }
        else
        {
            _enemyFase = EnemyFase.Standing;
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
        }
    }

    private void Shoot()
    {
        _timer += Time.deltaTime;
        if (_timer > 0.8f)
        {
            BulletPrefab.GetComponent<BulletsController>().Parent = Parent.EnemyShooter;

            Instantiate(BulletPrefab, transform.position, new Quaternion(0f, 0f, 0f, 0f));
            _timer = 0;
        }
        
    }

    private void TurnAround()
    {
        _player = GameObject.Find("Player(Clone)").transform;
        _direction = _player.position - transform.position;
        _angle = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg;
        _rigidbody.rotation = _angle; 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.layer)
        {
            case 6: // 6 = Player
                if (_enemyFase == EnemyFase.Standing) _enemyFase = EnemyFase.Shooting;
                break;
            case 7: // 7 = Bullet. Comprovar que la bala ve del player y el que la toca es el collider del quadrat.
                if (collision.gameObject.GetComponent<BulletsController>().Parent == Parent.Player && _boxCollider.IsTouching(collision))
                {
                    _life -= 5;
                    if (_enemyFase == EnemyFase.Moving) Resources.Load<DataPersistenceScriptableObject>("Data").ActualGamePoints += 5;
                    else if (_enemyFase == EnemyFase.Shooting || _enemyFase == EnemyFase.Standing) Resources.Load<DataPersistenceScriptableObject>("Data").ActualGamePoints += 10;
                    Destroy(collision.gameObject);
                }
                break;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        switch (collision.gameObject.layer)
        {
            case 6: // 6 = Player
                if (_enemyFase == EnemyFase.Standing) _enemyFase = EnemyFase.Shooting;
                break;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        switch (collision.gameObject.layer)
        {
            case 6: // 6 = Player
                if (_enemyFase == EnemyFase.Shooting) _enemyFase = EnemyFase.Moving;
                break;
        }
    }

    private void Die()
    {
        _spawnerController._enemysInScene--;
        Resources.Load<DataPersistenceScriptableObject>("Data").EnemyKilled++;
        Resources.Load<DataPersistenceScriptableObject>("Data").ActualGamePoints += 10;
        Destroy(this.gameObject);
    }


}
