using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float _positionMultiplier = 0.02f;
    private Vector3 _mousePosition;
    private float _angleMousePlayer;
    private Vector3 _playerPosition;

    public Weapons _actualWeapon; // enum de l'arma actual.
    private InventoryScriptableObject _inventaryWeapons; //Inventari amb la llista d'armes del player
    public  WeaponScriptableObject ActualWeaponSO;

    private Animator _animator;

    private float _actualLifePoints;

    

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdatedData();
        Moviment();

        if (Resources.Load<PlayerScriptableObject>("PlayerData").ActualLifePoints <= 0) Die();
    }

    private void Moviment()
    {
        // Change Horitzontal Axis
        if (Input.GetAxis("Horizontal") != 0)
        {
            _animator.SetBool("isWalking", true); // Active Animation
            transform.position = new Vector3(_positionMultiplier * Input.GetAxis("Horizontal") + transform.position.x, transform.position.y, transform.position.z);
        }
        else _animator.SetBool("isWalking", false); // Desactive Animation
        // Change Vertical Axis
        if (Input.GetAxis("Vertical") != 0)
        {
            _animator.SetBool("isWalking", true); // Active Animation
            transform.position = new Vector3(transform.position.x, _positionMultiplier * Input.GetAxis("Vertical") + transform.position.y, transform.position.z);
        }
        else if (Input.GetAxis("Horizontal") == 0) _animator.SetBool("isWalking", false); // Desactive Animation

        // Rotate object
        _mousePosition = Input.mousePosition;  // Vector3 de la posici� actual del ratol�.
        _playerPosition = Camera.main.WorldToScreenPoint(transform.position); // Posici� del player segons la seva posicio a la pantalla, no al mon.
        _mousePosition.x -= _playerPosition.x;
        _mousePosition.y -= _playerPosition.y;

        Animation();
    }

    private void Animation()
    {
        _animator.SetFloat("XInput", _mousePosition.x);
        _animator.SetFloat("YInput", _mousePosition.y);
    }

    public void ChangeWeapon(char letter)
    {
        switch (letter)
        {
            case 'x':
                if (_actualWeapon == Weapons.Pistola1) _actualWeapon = (Weapons) System.Enum.GetValues(typeof(Weapons)).Length - 1;
                else _actualWeapon--;
                break;
            case 'c':
                if (_actualWeapon == Weapons.Pistola5) _actualWeapon = 0;
                else _actualWeapon++;
                break;
        }
        ChangeWeaponOS();
    }

    private void ChangeWeaponOS()
    {
        switch (_actualWeapon)
        {
            case Weapons.Pistola1:
                ActualWeaponSO = Resources.Load<WeaponScriptableObject>("Pistolas/Pistola1");
                break;
            case Weapons.Pistola2:
                ActualWeaponSO = Resources.Load<WeaponScriptableObject>("Pistolas/Pistola2");
                break;
            case Weapons.Pistola3:
                ActualWeaponSO = Resources.Load<WeaponScriptableObject>("Pistolas/Pistola3");
                break;
            case Weapons.Pistola4:
                ActualWeaponSO = Resources.Load<WeaponScriptableObject>("Pistolas/Pistola4");
                break;
            case Weapons.Pistola5:
                ActualWeaponSO = Resources.Load<WeaponScriptableObject>("Pistolas/Pistola5");
                break;
        }
        GameObject.Find("Weapon").GetComponent<SpriteRenderer>().sprite = ActualWeaponSO.Sprite;
    }

    private void TakeWeapon()
    {

    }

    private void UpdatedData()
    {
        _actualLifePoints = Resources.Load<PlayerScriptableObject>("PlayerData").ActualLifePoints;
    }

    private void DashAction()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.layer)
        {
            case 7: // 7 = Bullet
                if (collision.gameObject.GetComponent<BulletsController>().Parent == Parent.EnemyShooter)
                {
                    Resources.Load<PlayerScriptableObject>("PlayerData").ActualLifePoints -= 5;
                    Destroy(collision.gameObject);
                }
                break;
        }
    }

    private void Die()
    {
        // No se deber�a eliminar porque da NullReference
        //Destroy(this.gameObject);
    }
}
