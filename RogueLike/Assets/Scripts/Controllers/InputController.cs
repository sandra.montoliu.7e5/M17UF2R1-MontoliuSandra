using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class InputController : MonoBehaviour
{
    [SerializeField] private WeaponController _weaponController;
    [SerializeField] private PlayerController _playerController;

    // Start is called before the first frame update
    void Start()
    {
        _weaponController = GameObject.Find("Weapon").GetComponent<WeaponController>();
        _playerController = GameObject.Find("Player(Clone)").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) _weaponController.Shoot(); // No sigue disparando aunque lo mantengas pulsado.
        //if (Input.GetMouseButtonDown(1)) ; // Dash
        //if (Input.GetKeyDown("space")) ; //Melee

        if (Input.GetKeyDown("x")) _playerController.ChangeWeapon('x');
        if (Input.GetKeyDown("c")) _playerController.ChangeWeapon('c');
        if (Input.GetAxis("Mouse ScrollWheel") < 0f) _playerController.ChangeWeapon('x');
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) _playerController.ChangeWeapon('c');

        
    }

    
}
