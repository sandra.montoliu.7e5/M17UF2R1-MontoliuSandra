using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerObject", menuName = "ScriptableObjects_Player")]
public class PlayerScriptableObject : ScriptableObject
{
    public float LifePoints = 100f;
    public float ActualLifePoints = 100f;
    public InventoryScriptableObject WeaponsInventory;

    private void OnEnable()
    {
        hideFlags = HideFlags.DontUnloadUnusedAsset;
    }
}
