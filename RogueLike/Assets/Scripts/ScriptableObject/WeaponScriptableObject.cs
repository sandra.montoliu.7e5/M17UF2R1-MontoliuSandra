using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Weapons
{
    Pistola1,
    Pistola2,
    Pistola3,
    Pistola4,
    Pistola5
}

/*public enum Projectiles
{
    Normal
}*/

[CreateAssetMenu(fileName = "WeaponObject", menuName = "ScriptableObjects_Weapon")]
public class WeaponScriptableObject : ScriptableObject
{
    public Weapons WeaponType;            // Tipo de arma / numero
    public string WeaponName;             // Nombre del arma
    public Sprite Sprite;                 // Sprite del arma
    //public Projectiles ProjectilesType;
    public float ForceProjectiles;        // Da�o que hacen sus balas
    public float SpeedProjectiles;        // Velocidad de las balas
    public int NumProjectiles;            // Numero de balas
    public int ActualNumProjectiles;
    public float ShootCadence;            // Tiempo de cadencia
}
