using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InventoryObject", menuName = "ScriptableObjects_Inventory")]
public class InventoryScriptableObject : ScriptableObject
{
    public List<WeaponScriptableObject> WeaponsInventory;

    private void OnEnable()
    {
        hideFlags = HideFlags.DontUnloadUnusedAsset;
    }
}
