using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DataPersistenceObject", menuName = "ScriptableObjects_Data")]
public class DataPersistenceScriptableObject : ScriptableObject
{
    public int LastGamePoints;
    public string LastGameInitials;

    public int BestGamePoints;
    public string BestGameInitials;

    public int ActualGamePoints;
    public string ActualGameInitials;

    public LastScene LastScene;
    public int Level;

    public int _maxEnemysSpawner;
    public int EnemyKilled;

    private void OnEnable() // Per a que no es reinici al recaregar l'escena.
    {
        hideFlags = HideFlags.DontUnloadUnusedAsset;
    }
}
