using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Unity.VisualScripting;
using System.IO;
using UnityEngine.Tilemaps;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public WeaponScriptableObject[] AllWeapons;
    private UnityEngine.Object _objectPlayerSO;
    private string _pathObjectPlayerSO;
    private UnityEngine.Object _objectInventorySO;
    private string _pathObjectInventorySO;

    
    private Tilemap tilemap;
    private Vector3Int tilemapLocalPosition;
    private Vector3 tilemapWorldPosition;
    private List<Vector3> _shootingPlaces;

    [SerializeField] private GameObject _player;
    private Vector3[] _startPositionPlayer;
    [SerializeField] private GameObject[] _maps;
    private int _indexMaps;

    private static GameManager instance = null;
    public static GameManager Instance { get { return instance; } }

    private void Awake()
    {
        
        switch (Resources.Load<DataPersistenceScriptableObject>("Data").LastScene)
        {  
            // Si l'ultima escena es la StartScene/GameOverScene vol dir que comencem un nou joc. Elimina totes les dades de les posibles partides anteriors.
            // Si l'ultima escena es GameScene vol dir que estem recaregan la escena per anar a un nou mapa.
            case LastScene.StartScene:
            case LastScene.GameOverScene:
                //Debug.Log("EnterCase1");
                // Crea nou SO per a les dades del player per comen�ar desde 0. Si ja existeix l'elimina.
                _objectPlayerSO = ScriptableObject.CreateInstance<PlayerScriptableObject>();
                _pathObjectPlayerSO = "Assets/Resources/PlayerData.asset";
                if (File.Exists(_pathObjectPlayerSO)) UnityEditor.AssetDatabase.DeleteAsset(_pathObjectPlayerSO);
                UnityEditor.AssetDatabase.CreateAsset(_objectPlayerSO, _pathObjectPlayerSO);
                // = pero per a l'inventari
                _objectInventorySO = ScriptableObject.CreateInstance<InventoryScriptableObject>();
                _pathObjectInventorySO = "Assets/Resources/Inventory.asset";
                if (File.Exists(_pathObjectInventorySO)) UnityEditor.AssetDatabase.DeleteAsset(_pathObjectInventorySO);
                UnityEditor.AssetDatabase.CreateAsset(_objectInventorySO, _pathObjectInventorySO);
                UnityEditor.AssetDatabase.SaveAssets();
                UnityEditor.AssetDatabase.Refresh();

                Resources.Load<PlayerScriptableObject>("PlayerData").ActualLifePoints = Resources.Load<PlayerScriptableObject>("PlayerData").LifePoints;
                Resources.Load<PlayerScriptableObject>("PlayerData").WeaponsInventory = Resources.Load<InventoryScriptableObject>("Inventory");
                Resources.Load<InventoryScriptableObject>("Inventory").WeaponsInventory = new List<WeaponScriptableObject>
                {
                    Resources.Load<WeaponScriptableObject>("Pistolas/Pistola1"),
                    //Resources.Load<WeaponScriptableObject>("Pistolas/Pistola2"),
                    //Resources.Load<WeaponScriptableObject>("Pistolas/Pistola3"),
                    //Resources.Load<WeaponScriptableObject>("Pistolas/Pistola4"),
                    //Resources.Load<WeaponScriptableObject>("Pistolas/Pistola5"),
                };
                //
                Resources.Load<DataPersistenceScriptableObject>("Data").Level = 1;
                Resources.Load<DataPersistenceScriptableObject>("Data").ActualGamePoints = 0;
                Resources.Load<DataPersistenceScriptableObject>("Data").EnemyKilled = 0;
                Resources.Load<DataPersistenceScriptableObject>("Data").LastScene = LastScene.GameScene;
                //
                gameObject.GetComponentInChildren<SpawnerController>()._enemysInScene = 0;

                AllWeapons = Resources.LoadAll<WeaponScriptableObject>("Pistolas"); // Inventari provisional
                break;
            case LastScene.GameScene:
            default:
                //Debug.Log("EnterCase2");
                break;
        }

        // Instanciar mapa aleatori
        _indexMaps = Random.Range(0, _maps.Length);
        Instantiate(_maps[_indexMaps], new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0));
    }

    // Start is called before the first frame update
    void Start()
    {
        // Instancia player
        _startPositionPlayer = GetTilesPositions("Tilemap_StartPoint").ToArray();
        Instantiate (_player, _startPositionPlayer[0], new Quaternion(0f, 0f, 0f, 0f));
    }

    // Update is called once per frame
    void Update()
    {
        GameObject.Find("Main Camera").transform.position = new Vector3(GameObject.Find("Player(Clone)").transform.position.x, GameObject.Find("Player(Clone)").transform.position.y,-10);

        GameOver();
    }

    public List<Vector3> GetTilesPositions(string NameTilemap)
    {
        // Get Tiles positions for the shooters to go and build their "canons"
        // https://forum.unity.com/threads/tilemap-tile-positions-assistance.485867/

        tilemap = GameObject.Find(NameTilemap).GetComponent<Tilemap>();
        _shootingPlaces = new List<Vector3>();

        for (int i = tilemap.cellBounds.xMin; i < tilemap.cellBounds.xMax; i++)
        { 
            for (int j = tilemap.cellBounds.yMin; j < tilemap.cellBounds.yMax; j++)
            {
                tilemapLocalPosition = new Vector3Int(i,j, (int)tilemap.transform.position.y);
                tilemapWorldPosition = tilemap.CellToWorld(tilemapLocalPosition);
                if (tilemap.HasTile(tilemapLocalPosition))
                {
                    _shootingPlaces.Add(tilemapWorldPosition);
                }
            }
        }
        return _shootingPlaces;
    }

    private void GameOver()
    {
        // Efecto muerte
        // FALTA
        if (Resources.Load<PlayerScriptableObject>("PlayerData").ActualLifePoints <= 0) SceneManager.LoadScene("GameOverScene");
    }


}
