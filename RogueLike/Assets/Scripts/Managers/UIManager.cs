using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Unity.VisualScripting;

public class UIManager : MonoBehaviour
{
    [SerializeField] private float _lifeScrollbar;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().name == "GameScene" || SceneManager.GetActiveScene().name == "ShopScene")
        {
            UpdateDataGameScene();
        }
    }

    private void UpdateDataGameScene()
    {
        _lifeScrollbar = Resources.Load<PlayerScriptableObject>("PlayerData").ActualLifePoints / Resources.Load<PlayerScriptableObject>("PlayerData").LifePoints;
        GameObject.Find("Life_scrollbar").GetComponent<Image>().fillAmount = _lifeScrollbar;
        ChangeColorLifeScrollbar();

        GameObject.Find("Life_text").GetComponent<Text>().text = Resources.Load<PlayerScriptableObject>("PlayerData").ActualLifePoints + " / " + Resources.Load<PlayerScriptableObject>("PlayerData").LifePoints;

        GameObject.Find("Points").GetComponent<Text>().text = Resources.Load<DataPersistenceScriptableObject>("Data").ActualGamePoints.ToString();
    }

    private void ChangeColorLifeScrollbar()
    {
        if (_lifeScrollbar >= 0.4f) GameObject.Find("Life_scrollbar").GetComponent<Image>().color = new Color32(93,225,171,255);
        else if (_lifeScrollbar >= 0.2f) GameObject.Find("Life_scrollbar").GetComponent<Image>().color = new Color32(225, 146, 52,255);
        else GameObject.Find("Life_scrollbar").GetComponent<Image>().color = new Color32(225, 60, 52,255);
    }

    /*private void LoadScene(string Scene)
    {
        SceneManager.LoadScene(Scene);
    }

    private void QuitApp()
    {
        //Solo funcionara en la aplicación construida, no en el editor.
        //Application.Quit();

        //Para que funcione en el editor:
        UnityEditor.EditorApplication.isPlaying = false;
    }*/
}
