using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ShopManager : MonoBehaviour
{
    [SerializeField] private GameObject _player;
    private Vector3[] _startPositionPlayer;

    private void Awake()
    {
        // Instancia player
        _startPositionPlayer = GetTilesPositions("Tilemap_StartPoint").ToArray();
        Instantiate(_player, _startPositionPlayer[0], new Quaternion(0f, 0f, 0f, 0f));
    }

    // Start is called before the first frame update
    void Start()
    {
        

        GameObject.Find("Door_Principal_Closed").GetComponent<DoorController>()._doorInteractuable = true;
    }

    // Update is called once per frame
    void Update()
    {
        GameObject.Find("Main Camera").transform.position = new Vector3(GameObject.Find("Player(Clone)").transform.position.x, GameObject.Find("Player(Clone)").transform.position.y, -10);
    }


    // CODIGO REPETIDO
    private Tilemap tilemap;
    private Vector3Int tilemapLocalPosition;
    private Vector3 tilemapWorldPosition;
    private List<Vector3> _shootingPlaces;
    public List<Vector3> GetTilesPositions(string NameTilemap)
    {
        // Get Tiles positions for the shooters to go and build their "canons"
        // https://forum.unity.com/threads/tilemap-tile-positions-assistance.485867/

        tilemap = GameObject.Find(NameTilemap).GetComponent<Tilemap>();
        _shootingPlaces = new List<Vector3>();

        for (int i = tilemap.cellBounds.xMin; i < tilemap.cellBounds.xMax; i++)
        {
            for (int j = tilemap.cellBounds.yMin; j < tilemap.cellBounds.yMax; j++)
            {
                tilemapLocalPosition = new Vector3Int(i, j, (int)tilemap.transform.position.y);
                tilemapWorldPosition = tilemap.CellToWorld(tilemapLocalPosition);
                if (tilemap.HasTile(tilemapLocalPosition))
                {
                    _shootingPlaces.Add(tilemapWorldPosition);
                }
            }
        }
        return _shootingPlaces;
    }
}
