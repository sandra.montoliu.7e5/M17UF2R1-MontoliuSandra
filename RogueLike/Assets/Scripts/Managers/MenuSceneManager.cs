using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum LastScene
{
    StartScene,
    GameScene,
    GameOverScene
}

public class MenuSceneManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        switch (SceneManager.GetActiveScene().name)
        {
            case "StartScene":
                GameObject.Find("BestGamePoints").GetComponent<Text>().text = Resources.Load<DataPersistenceScriptableObject>("Data").BestGamePoints.ToString();
                GameObject.Find("BestGameInitials").GetComponent<Text>().text = Resources.Load<DataPersistenceScriptableObject>("Data").BestGameInitials;
                GameObject.Find("LastGamePoints").GetComponent<Text>().text = Resources.Load<DataPersistenceScriptableObject>("Data").LastGamePoints.ToString();
                GameObject.Find("LastGameInitials").GetComponent<Text>().text = Resources.Load<DataPersistenceScriptableObject>("Data").LastGameInitials;
                Resources.Load<DataPersistenceScriptableObject>("Data").LastScene = LastScene.StartScene;
                break;
            case "GameOverScene":
                GameObject.Find("GamePoints").GetComponent<Text>().text = Resources.Load<DataPersistenceScriptableObject>("Data").ActualGamePoints.ToString();
                GameObject.Find("GameInitials").GetComponent<Text>().text = Resources.Load<DataPersistenceScriptableObject>("Data").ActualGameInitials;
                GameObject.Find("GameEnemies").GetComponent<Text>().text = Resources.Load<DataPersistenceScriptableObject>("Data").EnemyKilled.ToString();
                GameObject.Find("GameLevel").GetComponent<Text>().text = Resources.Load<DataPersistenceScriptableObject>("Data").Level.ToString();
                
                Resources.Load<DataPersistenceScriptableObject>("Data").LastScene = LastScene.GameOverScene;
                Resources.Load<DataPersistenceScriptableObject>("Data").LastGamePoints = Resources.Load<DataPersistenceScriptableObject>("Data").ActualGamePoints;
                Resources.Load<DataPersistenceScriptableObject>("Data").LastGameInitials = Resources.Load<DataPersistenceScriptableObject>("Data").ActualGameInitials;

                if (Resources.Load<DataPersistenceScriptableObject>("Data").ActualGamePoints > Resources.Load<DataPersistenceScriptableObject>("Data").BestGamePoints)
                {
                    Resources.Load<DataPersistenceScriptableObject>("Data").BestGamePoints = Resources.Load<DataPersistenceScriptableObject>("Data").ActualGamePoints;
                    Resources.Load<DataPersistenceScriptableObject>("Data").BestGameInitials = Resources.Load<DataPersistenceScriptableObject>("Data").ActualGameInitials;
                    GameObject.Find("BestGame").GetComponent<Text>().enabled = true;
                }
                break;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void LoadScene(string Scene)
    {
        SceneManager.LoadScene(Scene);
    }

    public void QuitApp()
    {
        //Solo funcionara en la aplicación construida, no en el editor.
        //Application.Quit();

        //Para que funcione en el editor:
        UnityEditor.EditorApplication.isPlaying = false;
    }

    public void NewGame()
    {
        LoadScene("GameScene");
    }

    public void GoMenu()
    {
        LoadScene("StartScene");
    }
}
